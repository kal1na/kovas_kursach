﻿using System;
using System.Collections.Generic;
using System.IO;
abstract class Information
{
    public string Name { get; set; }
    public int Key { get; set; }
    public Information(string name)
    {
        this.Name = name;
    }
    public abstract string Info();
}
class Driver : Information
{
    public string[] Schedule { get; set; }
    public int Experience { get; set; }
    public int Selery { get; set; }
    public string Level { get; set; }
    public string Direction { get; set; }
    public static int ID { get; set; }
    public Driver(string name, string level, int experience, string direction, int selery, string[] schedule) : base(name)
    {
        this.Level = level;
        this.Experience = experience;
        this.Selery = selery;
        this.Direction = direction;
        this.Schedule = schedule;
        this.Key = ID++;
    }
    public override string Info()
    {
        string data = Key + "|" + Name + "|" + Level + "|" + Experience + "|" + Direction + "|" + Selery + "|" + Schedule[0] + "|"+ Schedule[1] + "|"+ Schedule[2] + "|"+ Schedule[3];
        return data;
    }
}
class Route : Information
{
    public string Start { get; set; }
    public string End { get; set; }
    public string StartTime { get; set; }
    public string EndTime { get; set; }
    public static int ID { get; set; }
    public string Bus { get; set; }
    public String DriverKey { get; set; }
    public Route(string name, string start, string end, string starttime, string endtime, string bus, string key) : base(name)
    {
        this.Start = start;
        this.End = end;
        this.StartTime = starttime;
        this.EndTime = endtime;
        this.Bus = bus;
        this.DriverKey = key;
        this.Key = ID++;
    }
    public override string Info()
    {
        string data = Key + "|" + Name + "|" + Start + "|" + End + "|" + StartTime + "|" + EndTime + "|" + Bus + "|"+ DriverKey;
        return data;
    }
}
class Bus : Information
{
    public string Type { get; set; }
    public int Capacity { get; set; }
    public string Condition { get; set; }
    public static int ID { get; set; }
    public Bus(string name, string type, int capacity, string condition) : base(name)
    {
        this.Type = type;
        this.Capacity = capacity;
        this.Condition = condition;
        this.Key = ID++;
    }
    public override string Info()
    {
        string data = Key + "|" + Name + "|" + Type + "|" + Capacity + "|" + Condition;
        return data;
    }
}
namespace Kovas_kovas_kursach
{
    class Program
    {
        static void Main(string[] args)
        {
            string BusPath = @"/home/kalin/Documents/c#/Kovas/kovas_kursach/BigData/Buses.txt";
            string RoutePath = @"/home/kalin/Documents/c#/Kovas/kovas_kursach/BigData/Routes.txt";
            string DriverPath = @"/home/kalin/Documents/c#/Kovas/kovas_kursach/BigData/Drivers.txt";
            int buscount = Convert.ToInt32(Console.ReadLine());
            List<Bus> buses = new List<Bus>();
            Random rnd = new Random();
            string[] bools = new string[] {"true","false"};
            string[] types = new string[] {"Газель","Хюндай","Нисан","Хонда"};
            string[] bus = new string[] {"АХ","БУ","МЕ","ДР","ФЦ","ГУ","ЗА","КП","РЕ","РА","РМ","ИК","ИН","УС","НО","ГС","СГ","СЩ","СТ","ПТ","СА","ЮЗ","ЮП","КМ","УМ","МЯ","ЯД","ЯЗ","ЯК","ЯР","ЯФ","ФИ","ФУ","ФК","ФБ","ФХ"};
            for(int i = 0; i < buscount; i++)
            {
                buses.Add(new Bus(bus[rnd.Next(0,35)] + Convert.ToString(rnd.Next(1000,9999)) + bus[rnd.Next(0,35)], types[rnd.Next(0,4)], rnd.Next(20,60), bools[rnd.Next(0,2)]));
            }
            using (StreamWriter sw = new StreamWriter(BusPath, false, System.Text.Encoding.Default))
            {
                foreach (Bus b in buses)
                {
                    sw.WriteLine(b.Info());
                }
            }
            List<Route> routes = new List<Route>();
            int routecount = Convert.ToInt32(Console.ReadLine());
            string[] starts = new string[]{"7:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00"};
            string[] ends = new string[]{"15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00"};
            string[] startnames = new string[] {"СОборная","Кутузовская","Ришильевская","Андроновская","Македонская","Фонтанская","Алексеевская","Гординовская","Киргизкая","Амбулаторная","Ребиская","Паустовская","Коваленковская","Кесовая","Муратовская","Аргентиновская","Шапкинская","Чепушупская","Ломантиновская","Коригуанская","Слоновая","Северная","Менделеева","Тракторостроителей","Шевченко","Фауста","Екатерининская","Черноморская","Пиратова","Соболева","Сегоднешняя","Софиевская","Бочарова","Филатова","Торговая","Привоз","Сибирьская","Говорунова","Пировская",};//40
            for(int i = 0; i < routecount; i++)
            {
                routes.Add(new Route(bus[rnd.Next(0,35)] + rnd.Next(0,1000), startnames[rnd.Next(0,39)], startnames[rnd.Next(0,39)], starts[rnd.Next(0,7)], ends[rnd.Next(0,7)], Convert.ToString(buses[rnd.Next(0,buscount - 1)].Key), "Null"));
            }

            int drivecount = Convert.ToInt32(Console.ReadLine());
            List<Driver> drive = new List<Driver>();
            string[] levels = new string[] {"A","B","C","D","E","F"};
            string[] driverssurname = new string[] {"Коваленко","Терзи","Муратов","Иванов","Кутепов","Солонько","Тягнирядно","Башлаев","Македонский","Менделеев","Ковалев","Пастух","Наездник","Стрелок","Снайпер","Кафка","Артемович","Заболотный","Бочаров","Москаль"};
            string[] driversname = new string[] {"Андрей","Артем","Арсен","Егор","Генадий","Владислав","Инокентий","Юлиан","Максим","Рома","Сергей","Кирилл","Никита","Роберт","Алексей","Александр","Николай","Антон","Эдуард","Константин"};//20
            string[] driversfather = new string[] {"Станиславович","Владиславович","Вадимович","Олегович","Пантелеймонович","Владимирович","Сергеевич","Антонович","Романович","Константинович","Егорович","Иванович","Николаевич","Артемович","Робертович","Максимович","Генадьевич","Кириллович","Эдуардович","Александрович"};
            string[] week = new string[] {"пн","вт","ср","чт","пт","сб","вс"};

            for(int i = 0; i < drivecount; i++)
            {
                string[] doweek = new string[] {week[rnd.Next(0,7)],week[rnd.Next(0,7)],week[rnd.Next(0,7)],week[rnd.Next(0,7)]};
                drive.Add(new Driver(driversname[rnd.Next(0,19)] + "|" + driverssurname[rnd.Next(0,19)] + "|" + driversfather[rnd.Next(0,19)], levels[rnd.Next(0,5)], rnd.Next(10,30), routes[rnd.Next(0,routecount -1)].Name, rnd.Next(3000,7000), doweek));
            }
            using (StreamWriter sw = new StreamWriter(DriverPath, false, System.Text.Encoding.Default))
            {
                foreach (Driver d in drive)
                {
                    sw.WriteLine(d.Info());
                }
            }
            foreach(Driver d in drive)
            {
                d.Direction = routes[rnd.Next(0,routecount - 1)].Name;
            }
            using (StreamWriter sw = new StreamWriter(DriverPath, false, System.Text.Encoding.Default))
            {
                foreach (Driver d in drive)
                {
                    sw.WriteLine(d.Info());
                }
            }
            foreach(Route r in routes)
            {
                foreach(Driver d in drive)
                {
                    if(d.Direction == r.Name)
                    {
                        r.DriverKey = Convert.ToString(d.Key);
                    }
                }
            }
            using (StreamWriter sw = new StreamWriter(RoutePath, false, System.Text.Encoding.Default))
            {
                foreach (Route r in routes)
                {
                    sw.WriteLine(r.Info());
                }
            }
        }
    }
}
