using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleApp
{

abstract class Information
{
    public string Name { get; set; }  /////
    public int Key { get; set; }       /////
    public Information(string name)     /////Этот абстрактный клас был создан так как 
    {                                   ///// у всех последующих классов были схожие поля 
        this.Name = name;               ///// и требовалось выводить общюю информацию об объекте класса 
    }                                  /////
    public abstract string Info();    /////
}
class Driver : Information
{
    public string[] Schedule { get; set; } ///Класс для таблицы водителей 
    public int Experience { get; set; }    /// тут указаны все поля которые нудны в таблице
    public int Selery { get; set; }
    public string Level { get; set; }
    public string Direction { get; set; }
    public static int ID { get; set; }
    public Driver(string name, string level, int experience, string direction, int selery, string[] schedule) : base(name)
    {
        this.Level = level;
        this.Experience = experience;
        this.Selery = selery;
        this.Direction = direction;
        this.Schedule = schedule;
        this.Key = ID++;
    }
    public override string Info()
    {                                  // реализованый абстрактный метод
        string data = Key + "|" + Name + "|" + Level + "|" + Experience + "|" + Direction + "|" + Selery + "|" + Schedule[0] + "|"+ Schedule[1] + "|"+ Schedule[2] + "|"+ Schedule[3] + "|";
        return data;
    }
}
class Route : Information
{                                           /////Класс для таблицы рейсов где указаны все нужные поля для таблици
    public string Start { get; set; }
    public string End { get; set; }
    public string StartTime { get; set; }
    public string EndTime { get; set; }
    public static int ID { get; set; }
    public string Bus { get; set; }
    public string DriverKey { get; set; }
    public Route(string name, string start, string end, string starttime, string endtime, string bus, string key) : base(name)
    {
        this.Start = start;
        this.End = end;
        this.StartTime = starttime;
        this.EndTime = endtime;
        this.Bus = bus;
        this.DriverKey = key;
        this.Key = ID++;
    }
    public override string Info()
    {                                   ////Реализованый абстрактный метод
        string data = Key + "|" + Name + "|" + Start + "|" + End + "|" + StartTime + "|" + EndTime + "|" + Bus + "|"+ DriverKey;
        return data;
    }
}
class Bus : Information
{                                       ///////Класс дл таблицы Автоусов где указаны все поля для его таблицы 
    public string Type { get; set; }
    public int Capacity { get; set; }
    public bool Condition { get; set; }
    public static int ID { get; set; }
    public Bus(string name, string type, int capacity, bool condition) : base(name)
    {
        this.Type = type;
        this.Capacity = capacity;
        this.Condition = condition;
        this.Key = ID++;
    }
    public override string Info()
    {                                   ///РЕализованный абстрактный метод
        string data = Key + "|" + Name + "|" + Type + "|" + Capacity + "|" + Condition;
        return data;
    }
}
class BusOnRoute
{                       //// Класс "Автобус на маршуте"
    public int RouteKey { get; set; }
    public string StartTime { get; set; }
    public string EndTime { get; set; }
    public string BusName { get; set; }
    public string DriverKey { get; set; }
    public BusOnRoute(int RouteKey, string StartTime, string EndTime, string BusName, string DriverKey)
    {
        this.RouteKey = RouteKey;
        this.StartTime = StartTime;
        this.BusName = BusName;
        this.DriverKey = DriverKey;
        this.EndTime = EndTime;
    }

}
    public class Read
    {
        public static void Main(string[] args)
        {
            bool end = true;
            string BusPath = @"/home/kalin/Documents/c#/Kovas/kovas_kursach/BigData/Buses.txt";     /////
            string RoutePath = @"/home/kalin/Documents/c#/Kovas/kovas_kursach/BigData/Routes.txt"; /////// Пути к файлам
            string DriverPath = @"/home/kalin/Documents/c#/Kovas/kovas_kursach/BigData/Drivers.txt"; //////
            List<Bus> BusList = new List<Bus>();
            using (StreamReader sr = new StreamReader(BusPath, System.Text.Encoding.Default)) //Запись из файла в коллекцию 
            {
                string line;            
                while ((line = sr.ReadLine()) != null)
                {
                    string[] words = line.Split(new char[] { '|' }); ///Благодаря функции Split можно разрезать на массив прочитаную строку чтобы потом записать в конструктор
                    bool con;
                    if(words[4] == "true")
                    {
                        con = true;
                    }
                    else
                    {
                        con = false;
                    }
                    BusList.Add(new Bus(words[1],words[2],Convert.ToInt32(words[3]),con));
                }
            }
            List<Route> RouteList = new List<Route>();
            using (StreamReader sr = new StreamReader(RoutePath, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] words = line.Split(new char[] { '|' });
                    RouteList.Add(new Route(words[1],words[2],words[3],words[4],words[5],words[6],words[7]));
                }
            }
            List<Driver> DriverList = new List<Driver>();
            using (StreamReader sr = new StreamReader(DriverPath, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] words = line.Split(new char[] { '|' });
                    string[] graphick = new string[] {words[8],words[9],words[10],words[11]};
                    DriverList.Add(new Driver(words[1] + words[2] + words[3],words[4],Convert.ToInt32(words[5]),words[6],Convert.ToInt32(words[7]),graphick));
                }
            }
            while(end)
            {
                Console.WriteLine("1.List of drivers working on a particular route with an indication of their work schedule.");
                Console.WriteLine("2.Which buses serve this route.");
                Console.WriteLine("3.Which route is the driver with the maximum experience.");
                Console.WriteLine("4.Remove bus");
                Console.WriteLine("5.Add route");
                Console.WriteLine("6.Add driver");
                Console.WriteLine("7.Exit");
                switch (Convert.ToInt32(Console.ReadLine()))
                {
                    case 1:
                        foreach(var ro in RouteList)
                        {
                            Console.WriteLine(ro.Start); //Вывод всех нчальных станций
                        }
                        Console.WriteLine("Chose your start");
                        string started = Console.ReadLine();
                        foreach(var ro in RouteList)
                        {
                            Console.WriteLine(ro.End); // Вывод всех конечных станций
                        }
                        Console.WriteLine("Chose your end");
                        string ended = Console.ReadLine();
                        var selectOne = from i in RouteList
                        where i.Start == started & i.End == ended
                        select i.Bus;
                        int count = 0;
                        foreach (var i in selectOne)
                        {
                        Console.WriteLine("Bus key: " + i);
                        count++;
                        }
                        Console.WriteLine("These are all buses on the specified route, their number is: " + count);
                        count = 0;
                        break;
                    case 2:
                        foreach(var numb in RouteList)
                        {
                            Console.WriteLine(numb.Name); /// Вывод номеров маршрута
                        }
                        Console.WriteLine("Chose your direction");
                        string direct = Console.ReadLine();
                        var selectTwo = from i in RouteList
                        where i.Name == direct
                        select i.Bus;
                        int countof = 0;
                        foreach (var i in selectTwo)
                        {
                            if(i != "Null")
                            {
                                Console.WriteLine("Key of the bus: " + i); 
                                countof++;
                            }    
                        }
                        Console.WriteLine("Count of the bus: " + countof);          
                        break;
                    case 3:
                        int EXPERT = DriverList.Max( i => i.Experience );
                        var selectThree = from i in DriverList
                        where i.Experience == EXPERT
                        select i.Info();
                        foreach (var i in selectThree)
                        {
                            Console.WriteLine("Driver with the maximum experience: " + i);
                        }
                        break;
                    case 4:
                        Console.WriteLine("Enter the bus key to be deleted");
                        BusList.RemoveAt(Convert.ToInt32(Console.ReadLine()));
                        using (StreamWriter sw = new StreamWriter(BusPath, false, System.Text.Encoding.Default))
                        {
                            foreach (Bus r in BusList)
                            {
                                sw.WriteLine(r.Info());    /// ПЕрезапись файла
                            }
                        }
                        break;
                    case 5:
                        Console.WriteLine("Enter name");
                        string RouteName = Console.ReadLine();
                        Console.WriteLine("Enter start");
                        string RouteStart = Console.ReadLine();
                        Console.WriteLine("Enter end");
                        string RouteEnd = Console.ReadLine();
                        Console.WriteLine("Enter start time");
                        string StartTime = Console.ReadLine();
                        Console.WriteLine("Enter end time");
                        string EndTime = Console.ReadLine();
                        Console.WriteLine("Enter bus key");
                        string BusKey = Console.ReadLine();
                        Console.WriteLine("Enter driver key");
                        string RouteDriver = Console.ReadLine();
                        RouteList.Add(new Route(RouteName,RouteStart,RouteEnd,StartTime,EndTime,BusKey,RouteDriver));
                        using (StreamWriter sw = new StreamWriter(RoutePath, false, System.Text.Encoding.Default))
                        {
                            foreach (Route r in RouteList)
                            {
                                sw.WriteLine(r.Info());  //// Перезапись файла 
                            }
                        }
                        break;
                    case 6:
                        Console.WriteLine("Enter name");
                        string DriverName = Console.ReadLine();
                        Console.WriteLine("Enter level");
                        string DriveLevel = Console.ReadLine();
                        Console.WriteLine("Enter experience");
                        string DriverExpirience = Console.ReadLine();
                        Console.WriteLine("Enter Direction");
                        string DriverDirection = Console.ReadLine();
                        Console.WriteLine("Enter selery");
                        string DriverSelery = Console.ReadLine();
                        string[] DriverSchedule = new string[4];
                        for(int i = 0; i < 4; i++)
                        {
                            Console.WriteLine("Enter day of the week");
                            string schedule = Console.ReadLine();
                            DriverSchedule[i] = schedule;
                        }
                        DriverList.Add(new Driver(DriverName,DriveLevel,Convert.ToInt32(DriverExpirience),DriverDirection,Convert.ToInt32(DriverSelery),DriverSchedule));
                        using (StreamWriter sw = new StreamWriter(DriverPath, false, System.Text.Encoding.Default))
                        {
                            foreach (Driver r in DriverList)
                            {
                                sw.WriteLine(r.Info());   /// Перезапись файла
                            }
                        }
                        break;
                    case 7:
                        end = false;
                        break;
                }
            }
        }
    }
}